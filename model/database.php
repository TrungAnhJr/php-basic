<?php
    class DB {
        protected $host = "localhost";
        protected $db_name = "php-basic";
        protected $username = "homestead";
        protected $password = "secret";
        
        protected $mysqli;

        public function __construct() 
        {
            $this->mysqli = mysqli_connect('localhost', 'homestead', 'secret', 'php-basic');
        }
    }
?>