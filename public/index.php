<?php
require "../controller/controller.php";

$index = new Controller();
$getID = explode('/', $_SERVER['REQUEST_URI']);
$id = array_pop($getID);

switch ($_SERVER['REQUEST_URI']) {
    case "/view/add": {
        $index->Add();
        break;
    }
    case "/view/edit/$id": {
        $index->Edit();
        break;
    }  
    case "/view/delete/$id": {
        $index->Delete();
        break;
    } 
    default: {
        $index->Index();
        break;
    } 
}


?>
