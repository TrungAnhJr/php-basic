<?php
require "../model/model.php";

    class Controller
    {
        public $data;

        public function Index() 
        {
            $db = new Model();
            $this->data = $db->getData();
            require_once "../view/view.php";  
        } 

        public function Add() 
        {
            require_once "../view/add.php";
            if(isset($_POST['submit'])) {
                $name = $_POST['nameAdd'];
                $db = new Model();
                $this->data = $db->add($name);
                header('Location: /');
            }   
        } 

        public function Edit() 
        {
            require_once "../view/edit.php";
            if(isset($_POST['submit'])) {
                $name = $_POST['nameEdit'];
                $getID = explode('/', $_SERVER['REQUEST_URI']);
                $id = array_pop($getID);
                $db = new Model();
                $this->data = $db->edit($id, $name);
                header('Location: /');
            }
        }

        public function Delete() 
        {
            $getID = explode('/', $_SERVER['REQUEST_URI']);
            $id = array_pop($getID);
            if(isset($id)) {
                $db = new Model();
                $this->data = $db->delete($id);
                header('Location: /');
            }
        } 
    }
?>