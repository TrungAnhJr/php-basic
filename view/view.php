<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>PHP-Basic</title>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-BmbxuPwQa2lc/FVzBcNJ7UAyJxM6wuqIj61tLrc4wSX0szH/Ev+nYRRuWlolflfl" crossorigin="anonymous">
</head>
<body>
    <div class="userList">
        <h3>USER LIST</h3>
        <a href="view/add" class="btn btn-success">Add</a>
        <table class="table table-striped">
            <thead>
                <th>Name</th>
                <th>Action</th>       
            </thead>
            <tbody>
                <?php foreach($this->data as $datas) {
                ?>
                <tr>
                    <td><?=$datas['name'] ?></td>
                    <td>
                        <a href="view/edit/<?=$datas['id'] ?>" class="btn btn-primary">Edit</a>
                        <a href="view/delete/<?=$datas['id'] ?>" class="btn btn-danger">Delete</a>
                    </td>
                </tr>
                <?php
                }  
                ?>
            </tbody>
        </table>
    </div>
</body>
</html>